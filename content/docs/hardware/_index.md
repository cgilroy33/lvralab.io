---
weight: 50
title: Hardware
---

# Hardware

A non comprehensive table of various VR/XR devices and the drivers that support them.

| Device               | [SteamVR](/docs/steamvr/)             | [Monado](/docs/fossvr/monado/) | [WiVRn](/docs/fossvr/wivrn/) |
|----------------------|:-------------------------------------:|:------------------------------:|:----------------------------:|
| **PC VR**            |                                       |                                |                              |
| Valve Index          | ✅                                    | ✅                             | --                           |
| HTC Vive             | ✅                                    | ✅                             | --                           |
| HTC Vive Pro         | ✅                                    | ✅                             | --                           |
| HTC Vive Pro 2       | ✅ (custom [driver](https://github.com/CertainLach/VivePro2-Linux-Driver)) | --                             | --                           |
| Pimax 4K             | ?                                     | ❌                             | ?                            |
| Pimax 5K Plus        | ?                                     | ❌                             | ?                            |
| Pimax 8K             | ?                                     | ❌                             | ?                            |
| **Standalone**       |                                       |                                |                              |
| Quest                | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Quest 2              | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Quest Pro            | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ?                            |
| Quest 3              | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ?                            |
| Pico 4               | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                            |
| Pico Neo 3           | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                            |
| HTC Vive Focus 3     | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ?                            |
| HTC Vive XR Elite    | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ?                            |
| Lynx R1              | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ?                            |
| **Trackers**         |                                       |                                |                              |
| Vive/Tundra trackers | ✅                                    | ✅                             | ?                            |
| SlimeVR trackers     | ✅                                    | ❌                             | ?                            |
