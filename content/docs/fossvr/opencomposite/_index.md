---
weight: 800
title: OpenComposite
---

# OpenComposite

- [OpenComposite GitLab repository](https://gitlab.com/znixian/OpenOVR)

> OpenComposite OpenXR (previously known as OpenOVR - OpenVR for OculusVR - but renamed due to confusion with OpenVR) is an implementation of SteamVR's API - OpenVR, forwarding calls directly to the OpenXR runtime. Think of it as a backwards version of ReVive, for the OpenXR compatible headsets.
>
> This allows you to play SteamVR-based games on an OpenXR compatible headset as though they were native titles, without the use of SteamVR!

Please note the OpenVR implementation is incomplete and contains only what's necessary to run most games for compatibility. If you plan to implement software, utilize the OpenXR API, specification [here](https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html).

Contributions to improve the OpenVR to OpenXR mapping are welcome.
