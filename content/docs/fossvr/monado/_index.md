---
weight: 100
title: Monado
---

# Monado

- [Monado home page](https://monado.freedesktop.org/)
- [Monado GitLab repository](https://gitlab.freedesktop.org/monado/monado)

> Monado is an open source XR runtime delivering immersive experiences such as VR and AR on mobile, PC/desktop, and other devices. Monado aims to be a complete and conformant implementation of the OpenXR API made by Khronos. The project is currently being developed for GNU/Linux and aims to support other operating systems such as Windows in the near future.

Essentially, Monado is an open source OpenXR implementation, it can be used as an alternative to SteamVR.

Depending on the game, Monado can offer a better overall experience (if with less features) compared to SteamVR, or it might not work at all.

Monado's space may be modified with the enviornment variables OXR_TRACKING_ORIGIN_OFFSET_X=0.0, OXR_TRACKING_ORIGIN_OFFSET_Y=1.0, OXR_TRACKING_ORIGIN_OFFSET_Z=0.0 to force a defined space or origin offset throughout the session. Useful for seated mode configurations, offset units are floating point in meters, positive or negative.

Monado is made for PCVR headsets, if you have a standalone headset you can check out [WiVRn](/docs/fossvr/wivrn/) or [ALVR](/docs/steamvr/alvr/).

## Steam

To use Monado as the OpenXR runtime with Steam, or if you're planning to use the SteamVR lighhouse driver in Monado, make sure to [run the room setup first](/docs/steamvr/).


## GPU support matrix

| Manufacturer | Driver                   | Vulkan Support | DisplayPort Audio | Reprojection Support           | Hybrid Graphics Support | Notes                                                              |
|--------------|--------------------------|----------------|-------------------|-------------------------------|-------------------------|--------------------------------------------------------------------|
| Nvidia       | Proprietary              | Very poor      | Yes               | Functional, non-robust        | Crashes                 | ~~Notable `glcore` crashes with OpenComposite. Proton VR games not playable.~~ There is a new patch available that may solve this problem, more details [here](https://discord.com/channels/1065291958328758352/1065360238716407939/1210006800007692358)           |
| Nvidia       | Nouveau + NVK Vulkan (Open Source) | Functional    | No                | No proper Vulkan queue        | Untested                | Lacks DisplayPort audio, suffers from stutter without reprojection queue. |
| Intel        | ANV (Open Source)        | Excellent      | Yes               | Functional, non-robust        | Functional              | Dedicated cards recommended, lacks robust reprojection like AMD.   |
| AMD          | RADV (Open Source)       | Excellent      | Yes               | Robust (RDNA and up)          | Functional              | RDNA generation and up supported with compute tunneling for reprojection. Lower than RDNA are not robust. |

**Notes:**
- **Vulkan Support**: Indicates the level of Vulkan API stability.
- **DisplayPort Audio**: Whether audio over DisplayPort is supported.
- **Reprojection Support**: Describes the support and quality of reprojection features for VR. Poor support indicates that the driver is not able to properly handle Vulkan realtime shaders and it will present as visual stutter. Non-robust solutions will suffer stutter under very high GPU load.
- **PRIME/ Hybrid GPU Support**: Compatibility with systems using multiple GPUs simultaneously. Monado and all clients must be run on a single select GPU due to memory tiling requirements.
- For Nvidia proprietary drivers, only native Linux XR applications are stable. OpenComposite leads to crashes.
- AMD GPUs lower than RDNA generation have functional but less robust reprojection capabilities, expected to be similar to Intel.
- PRIME configurations are generally functional across all but Nvidia's proprietary drivers, which are known to crash with OpenComposite for Steam applications. The open-source Nvidia PRIME support remains untested.
- Audio over displayport is known to temporarily cut out when new audio sources spring up on pipewire.
- X11 configurations are highly discouraged but possible to run, please upgrade your system to Wayland if at all possible.